﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Controller;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Diagnostics;
using Clock.Utils;

namespace Clock.Controller.Tests
{
    [TestClass()]
    public class TimeTrackerTests
    {
        protected IProject project;
        protected IWorkspanPool workspanPool;
        protected IProjectPool projectPool;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {

        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
        }
        [TestInitialize]
        public void TestInitialize()
        {
            workspanPool = new WorkspanPool();
            projectPool = new ProjectPool();
            project = new Project { Name = "Nereid" };
        }
        [TestCleanup]
        public void TestCleanup()
        {
            IEnumerable<IWorkspan> workspans = workspanPool.GetByProject(project);
            foreach(IWorkspan span in workspans)
            {
                workspanPool.Remove(span);
            }
            projectPool.Remove(project);
        }
        [TestMethod()]
        public void CanCreateTest()
        {
            ITimeTracker tracker = new TimeTracker(TimeTracker.CreateDefaultStopwatch, TimeProvider.GetCurrentTime);
            Assert.AreEqual(tracker.IsPaused, true);
        }

        [TestMethod()]
        public void CanSetAndReadProject()
        {
            ITimeTracker tracker = new TimeTracker(TimeTracker.CreateDefaultStopwatch, TimeProvider.GetCurrentTime);
            IProject project = new Project { Name = "Eris" };
            tracker.Project = project;

            Assert.AreEqual(tracker.Project, project);
        }


        [TestMethod()]
        public void CanSetAndReadDescription()
        {
            ITimeTracker tracker = new TimeTracker(TimeTracker.CreateDefaultStopwatch, TimeTracker.DefaultTimeProvider);
            string desc = "a;hlgajfg";

            tracker.Description = desc;

            Assert.AreEqual(tracker.Description, desc);
        }
        [TestMethod()]
        public void CanCountTimeAfterStart()
        {
            ITimeTracker tracker = new TimeTracker(FakeStopwatch.MakeFakeStopwatch, FakeStopwatch.GetFakeTime);
            tracker.Project = project;
            TimeSpan halfHourTime = new TimeSpan(0, 30, 0);
            HoursMinutes halfHour = new HoursMinutes(0, 30);

            tracker.Start();
            FakeStopwatch.AdvanceTime(halfHourTime);
            HoursMinutes elapsed = tracker.CurrentWorkingTime;

            IWorkspanPool pool = new WorkspanPool();
            Assert.AreEqual(elapsed, halfHour);

        }
        [TestMethod()]
        public void CanCountPauseTime()
        {
            ITimeTracker tracker = new TimeTracker(FakeStopwatch.MakeFakeStopwatch, FakeStopwatch.GetFakeTime);
            TimeSpan halfHourTime = new TimeSpan(0, 30, 0);
            HoursMinutes halfHour = new HoursMinutes(0, 30);
            tracker.Project = project;

            tracker.Start();
            FakeStopwatch.AdvanceTime(halfHourTime);
            tracker.Pause();
            FakeStopwatch.AdvanceTime(halfHourTime);
            tracker.Start();
            HoursMinutes elapsedPause = tracker.CurrentPauseTime;

            Assert.AreEqual(elapsedPause, halfHour);
        }

        [TestMethod()]
        public void CanPause()
        {
            ITimeTracker tracker = new TimeTracker(TimeTracker.CreateDefaultStopwatch, FakeStopwatch.GetFakeTime);
            TimeSpan span = new TimeSpan(0, 5, 0);
            tracker.Project = project;
            tracker.Start();
            FakeStopwatch.AdvanceTime(span);

            tracker.Pause();

            Assert.AreEqual(tracker.IsPaused, true);
        }

        [TestMethod()]
        public void CanSaveFinishedWorkspan()
        {
            ITimeTracker tracker = new TimeTracker(FakeStopwatch.MakeFakeStopwatch, FakeStopwatch.GetFakeTime);
            tracker.Project = project;
            HoursMinutes halfHour = new HoursMinutes(0, 30);
            TimeSpan halfHousSpan = new TimeSpan(0, 30, 0);

            tracker.Start();
            FakeStopwatch.AdvanceTime(halfHousSpan);
            tracker.Finish();

            IEnumerable< IWorkspan> spans = workspanPool.GetByProject(project);
            foreach(IWorkspan span in spans)
            {
                workspanPool.Remove(span);
            }
            
            Assert.AreEqual(1, spans.Count());
            IWorkspan workspan = spans.First();
            Assert.AreEqual(workspan.Project, project);
            Assert.AreEqual(workspan.Worktime, halfHour);

            
        }
        
    }
    public class FakeStopwatch : Stopwatch, IStopwatch
    {
        protected static DateTime savedTime = DateTime.MinValue;
        public static void AdvanceTime (TimeSpan time)
        {
            savedTime += time;
        }
        public static DateTime GetFakeTime()
        {
            return savedTime;
        }
        public new TimeSpan Elapsed
        {
            get
            {
                //throw new Exception("fuck you");
                return new TimeSpan(0, 30, 0);
            }
        }
        public static IStopwatch MakeFakeStopwatch()
        {
            return new FakeStopwatch();
        }
        public static IStopwatch MakeSimpleStopwatch()
        {
            return new FakeStopwatch();
        }
    }
}