﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Configuration;

namespace ClockTests.Utilities
{
    class ConfigManager
    {
        protected static Config RealConfig {get; set;}
        protected static Config FakeConfig { get; set; }
        public static void MakeFakeConfig()
        {
            RealConfig = Config.Load();

            FakeConfig = new Config
            {
                DbName = "Test",
                DbPass = "pass",
                DbUser = "sa",
                DbPath = @"Hell\SQLEXPRESS"
            };

            FakeConfig.Save();
        }

        public static void RestoreRealConfig()
        {
            if (RealConfig == null)
                throw new Exception("Oh fuck");
            RealConfig.Save();
        }
    }
}
