﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Utils;

namespace Clock.Model.Tests
{
    [TestClass()]
    public class HoursMinutesTests
    {
        [TestMethod()]
        public void ZeroArgumentConstructorGivesZeroHoursZeroMinutes()
        {
            HoursMinutes time = new HoursMinutes();
            Assert.AreEqual(time.Hours, 0);
            Assert.AreEqual(time.Minutes, 0);
        }

        [TestMethod()]
        public void TwoArgumentConstructor()
        {
            int hours = 4;
            int minutes = 35;

            HoursMinutes time = new HoursMinutes(hours, minutes);

            Assert.AreEqual(hours, time.Hours);
            Assert.AreEqual(minutes, time.Minutes);
        }
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CannotCreateWithMinutesHigherThan59()
        {
            int hours = 1;
            int minutes = 60;

            HoursMinutes time = new HoursMinutes(hours, minutes);
        }
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CannotAssignMinutesHigherThan59()
        {
            HoursMinutes time = new HoursMinutes();

            time.Minutes = 60;
        }
        [TestMethod()]
        public void ToStringTest()
        {
            int hours = 1;
            int minutes = 3;

            HoursMinutes time = new HoursMinutes(hours, minutes);
            string testString = time.ToString();

            Assert.AreEqual(testString, "01:03");
        }
        [TestMethod()]
        public void PlusOperatorSimpleTest()
        {
            HoursMinutes oneArgument = new HoursMinutes(2, 6);
            HoursMinutes secondArgument = new HoursMinutes(3, 12);

            HoursMinutes sum = oneArgument + secondArgument;

            Assert.AreEqual(sum.Hours, 5);
            Assert.AreEqual(sum.Minutes, 18);
        }
        [TestMethod()]
        public void PlusOperatorWithMoreThan50TotalMinutes()
        {
            HoursMinutes oneArgument = new HoursMinutes(1, 40);
            HoursMinutes secondArgument = new HoursMinutes(1, 50);

            HoursMinutes sum = oneArgument + secondArgument;

            Assert.AreEqual(sum.Hours, 3);
            Assert.AreEqual(sum.Minutes, 30);
        }
    }
}