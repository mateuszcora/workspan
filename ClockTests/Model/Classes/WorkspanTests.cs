﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Clock.Utils;

namespace Clock.Model.Tests
{
    [TestClass()]
    public class WorkspanTests
    {

        [TestMethod()]
        public void GetWorktime()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 13, 0, 0);

            Workspan workspan = new Workspan(start, end);
            HoursMinutes time = workspan.Worktime;

            Assert.AreEqual(time.Hours, 1);
            Assert.AreEqual(time.Minutes, 0);
        }
        [TestMethod()]
        public void TakesTwoConstructorParameters()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 13, 0, 0);

            Workspan workspan = new Workspan(start, end);

            Assert.AreEqual(workspan.StartTime, start);
            Assert.AreEqual(workspan.EndTime, end);
        }
        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void RequiresEndTimeLaterThanStart()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 0, 0);

            Workspan workspan = new Workspan(start, end);
        }
        [TestMethod()]
        public void ElapsedTimeEqualsOneHour()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 13, 0, 0);

            Workspan workspan = new Workspan(start, end);

            Assert.AreEqual(workspan.Hours, 1);
        }
        [TestMethod()]
        public void ElapsedTimeEqualsOneMinute()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);

            Workspan workspan = new Workspan(start, end);

            Assert.AreEqual(workspan.Minutes, 1);
        }
        [TestMethod()]
        public void DescriptionAssignment()
        {
            string description = "What the workspan was about";
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);

            Workspan workspan = new Workspan(start, end);
            workspan.Description = description;

            Assert.AreEqual(description, workspan.Description);
        }
        [TestMethod()]
        public void ProjectAssignment()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);
            Project project = new Project();

            Workspan workspan = new Workspan(start, end);
            workspan.Project = project;

            Assert.AreEqual(project, workspan.Project);

        }

    }
}