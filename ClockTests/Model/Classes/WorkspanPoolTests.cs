﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using ClockTests.Utilities;
using Clock.Configuration;

namespace Clock.Model.Tests
{
    [TestClass()]
    public class WorkspanPoolTests
    {
        protected static ISessionFactory sessionFactory;
        protected static NHibernate.Cfg.Configuration configuration;

        protected IWorkspan[] initialWorkspans;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            ConfigManager.MakeFakeConfig();
            Config config = Config.Load();
            configuration = new NHibernate.Cfg.Configuration();
            configuration.Configure();
            configuration.SetProperty("connection.connection_string", config.ConnectionString);
            configuration.AddAssembly(typeof(Project).Assembly);
            sessionFactory = configuration.BuildSessionFactory();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            ConfigManager.RestoreRealConfig();
        }
        [TestInitialize]
        public void TestInitialize()
        {
            new SchemaExport(configuration).Execute(false, true, false);
            CreateInitialWorkspans();
        }
        /*
         * to be added later once I figure out how to make sure every workspan has project
         * what if I try to add workspan with project that is not saved to db? open another session and check? 
         * Won't it loop (project chceck workspan, workspan checks object and so on)
         * 
        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void CreatingWithNoProjectAssignedThrowsException()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);
            IWorkspanPool workspans = new WorkspanPool();
            IWorkspan workspan = new Workspan(start, end);

            workspans.Create(workspan);

        }
        */

        [TestMethod()]
        public void CanCreate()
        {
            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);
            IProjectPool projectPool = new ProjectPool();
            IProject project = new Project { Name = "Venus" };
            projectPool.Create(project);


            IWorkspanPool workspans = new WorkspanPool();
            IWorkspan workspan = new Workspan(start, end);
            workspan.Project = project;

            workspans.Create(workspan);
        }

        [TestMethod()]
        public void CanCreateRange()
        {
            IWorkspanPool workspans = new WorkspanPool();
            IProjectPool projectPool = new ProjectPool();
            IProject project = new Project { Name = "Venus" };
            projectPool.Create(project);

            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);
            IWorkspan workspan = new Workspan(start, end);
            workspan.Project = project;

            DateTime start2 = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end2 = new DateTime(2018, 1, 1, 12, 1, 0);
            IWorkspan workspan2 = new Workspan(start2, end2);
            workspan2.Project = project;

            workspans.Create(new List<IWorkspan> { workspan, workspan2 });

            IWorkspan readFromDb1 = workspans.GetByID(workspan.ID);
            IWorkspan readFromDb2 = workspans.GetByID(workspan2.ID);

            AssertWorkspansHaveSameData(readFromDb1, workspan);
            AssertWorkspansHaveSameData(readFromDb2, workspan2);
        }
        
        [TestMethod()]
        [ExpectedException(typeof(Exception))]
        public void CreatingRangeWithIncorrectWorkspanMakesNoChanges()
        {
            IWorkspanPool pool = new WorkspanPool();
            IWorkspan workspan = new Workspan();

            pool.Create(workspan);

        }

        [TestMethod()]
        public void CanUpdate()
        {
            IWorkspan existing = initialWorkspans[0];
            IWorkspanPool pool = new WorkspanPool();
            string newDescription = "New description";
            existing.Description = newDescription;

            pool.Update(existing);

            IWorkspan readFromDb = pool.GetByID(existing.ID);
            AssertWorkspansHaveSameData(readFromDb, existing);
        }

        [TestMethod()]
        public void CanGetExistingGetByID()
        {
            IWorkspan existing = initialWorkspans[0];
            IWorkspanPool workspanPool = new WorkspanPool();

            IWorkspan readFromDb = workspanPool.GetByID(existing.ID);

            AssertWorkspansHaveSameData(readFromDb, existing);
        }
        [TestMethod()]
        public void GettingWorkspanByNonExistingIdReturnsNull()
        {
            Guid guid = Guid.NewGuid();
            IWorkspanPool workspanPool = new WorkspanPool();

            IWorkspan readFromDb = workspanPool.GetByID(guid);

            Assert.AreEqual(readFromDb, null);
        }

        protected void CreateInitialWorkspans()
        {
            IProjectPool projectPool = new ProjectPool();
            Project Saturn = new Project
            {
                Name = "Saturn"
            };
            Project Jupiter = new Project
            {
                Name = "Jupiter"
            };
            projectPool.Create(Saturn);
            projectPool.Create(Jupiter);

            IWorkspanPool workspanPool = new WorkspanPool();

            DateTime start = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime end = new DateTime(2018, 1, 1, 12, 1, 0);
            IWorkspan Titan = new Workspan(start, end);
            Titan.Project = Saturn;

            start = new DateTime(2018, 1, 1, 14, 0, 0);
            end = new DateTime(2018, 1, 1, 15, 1, 0);
            IWorkspan Rhea = new Workspan(start, end);
            Rhea.Project = Saturn;

            start = new DateTime(2018, 1, 1, 11, 0, 0);
            end = new DateTime(2018, 1, 1, 15, 1, 0);
            IWorkspan Iapetus = new Workspan(start, end);
            Iapetus.Project = Saturn;


            start = new DateTime(2018, 1, 1, 5, 0, 0);
            end = new DateTime(2018, 1, 1, 12, 1, 0);
            IWorkspan Ganymede = new Workspan(start, end);
            Ganymede.Project = Jupiter;

            start = new DateTime(2018, 1, 1, 14, 0, 0);
            end = new DateTime(2018, 1, 1, 20, 1, 0);
            IWorkspan Callisto = new Workspan(start, end);
            Callisto.Project = Jupiter;

            start = new DateTime(2018, 1, 1, 11, 0, 0);
            end = new DateTime(2018, 1, 1, 15, 50, 0);
            IWorkspan Io = new Workspan(start, end);
            Io.Project = Jupiter;

            workspanPool.Create(Titan);
            workspanPool.Create(Rhea);
            workspanPool.Create(Iapetus);
            workspanPool.Create(Ganymede);
            workspanPool.Create(Callisto);
            workspanPool.Create(Io);

            initialWorkspans = new[] { Titan, Rhea, Iapetus, Ganymede, Callisto, Io };
        }
        [TestMethod()]
        public void CanGetByProject()
        {
            IWorkspanPool pool = new WorkspanPool();
            IProject project = initialWorkspans[0].Project;

            IEnumerable<IWorkspan> readFromDb = pool.GetByProject(project);

            foreach(IWorkspan wspan in readFromDb)
            {
                Assert.AreEqual(project.Name, wspan.Project.Name);
                Assert.AreEqual(project.ID, wspan.Project.ID);
            }
            Assert.AreEqual(3, readFromDb.Count());
        }
        [TestMethod()]
        public void CanDelete()
        {
            IWorkspan workspan = initialWorkspans[0];
            IWorkspanPool pool = new WorkspanPool();

            pool.Remove(workspan);

            IWorkspan readFromDb = pool.GetByID(workspan.ID);

            Assert.AreEqual(readFromDb, null);

        }
        /// <summary>
        /// The readFromDb workspan is checked for being null, the existing one isn't
        /// </summary>
        protected void AssertWorkspansHaveSameData(IWorkspan readFromDb, IWorkspan existing)
        {
            Assert.AreNotEqual(readFromDb, null);
            Assert.AreNotEqual(readFromDb, existing);
            Assert.AreEqual(readFromDb.ID, existing.ID);
            Assert.AreEqual(readFromDb.StartTime, existing.StartTime);
            Assert.AreEqual(readFromDb.EndTime, existing.EndTime);
            Assert.AreEqual(readFromDb.Description, existing.Description);
        }


    }
}