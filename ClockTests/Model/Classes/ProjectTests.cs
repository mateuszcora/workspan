﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Clock.Utils;

namespace Clock.Model.Tests
{
    [TestClass()]
    public class ProjectTests
    {
        protected static ISessionFactory sessionFactory;
        protected static NHibernate.Cfg.Configuration configuration;

        protected IWorkspan[] initialWorkspans;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            configuration = new NHibernate.Cfg.Configuration();
            configuration.Configure();
            configuration.AddAssembly(typeof(WorkspanPool).Assembly);
            sessionFactory = configuration.BuildSessionFactory();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {

        }
        [TestInitialize]
        public void TestInitialize()
        {
            new SchemaExport(configuration).Execute(false, true, false);
        }
        [TestMethod()]
        public void WorktimeShouldBeSumOfWorkspans()
        {
            Project project = new Project
            {
                Name = "Mars"
            };
            DateTime startOne = new DateTime(2018, 1, 1, 12, 0, 0);
            DateTime endOne = new DateTime(2018, 1, 1, 12, 30, 0);
            DateTime startTwo = new DateTime(2018, 1, 1, 14, 0, 0);
            DateTime endTwo = new DateTime(2018, 1, 1, 14, 10, 0);

            IWorkspan workspanOne = new Workspan(startOne, endOne);
            workspanOne.Project = project;
            IWorkspan workspanTwo = new Workspan(startTwo, endTwo);
            workspanTwo.Project = project;

            IProjectPool projectPool = new ProjectPool();
            IWorkspanPool workspanPool = new WorkspanPool();

            projectPool.Create(project);
            workspanPool.Create(workspanOne);
            workspanPool.Create(workspanTwo);

            HoursMinutes total = project.TotalWorkTime;

            Assert.AreEqual(total.Hours, 0);
            Assert.AreEqual(total.Minutes, 40);
        }
    }
}