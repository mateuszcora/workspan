﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Model.Tests
{
    [TestClass()]
    public class GuidedRecordTests
    {
        [TestMethod()]
        public void ConstructorTest()
        {
            GuidedRecord record = new GuidedRecord();
            Assert.AreNotEqual(record, null);
        }
        [TestMethod()]
        public void GetGuidTest()
        {
            GuidedRecord record = new GuidedRecord();
            Guid guid = record.ID;
            Assert.AreNotEqual(guid, null);
        }
    }
}