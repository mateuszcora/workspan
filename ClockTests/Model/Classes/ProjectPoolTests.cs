﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;
using ClockTests.Utilities;
using Clock.Configuration;

namespace Clock.Model.Tests
{
    [TestClass()]
    public class ProjectPoolTests
    {
        protected static ISessionFactory sessionFactory;
        protected static NHibernate.Cfg.Configuration configuration;

        protected readonly IProject[] initialProjects = new[]
        {
            new Project{Name = "Mars"},
            new Project{Name = "Deimos"},
            new Project{Name = "Phobos"}
        };
        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            ConfigManager.MakeFakeConfig();
            Config config = Config.Load();
            configuration = new NHibernate.Cfg.Configuration();
            configuration.Configure();
            configuration.SetProperty("connection.connection_string", config.ConnectionString);
            configuration.AddAssembly(typeof(Project).Assembly);
            sessionFactory = configuration.BuildSessionFactory();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            ConfigManager.RestoreRealConfig();
        }
        [TestInitialize]
        public void TestInitialize()
        {
            new SchemaExport(configuration).Execute(false, true, false);
            CreateInitialProjects();
        }
        [TestMethod()]
        public void CanCreateProjectPoolTest()
        {
            IProjectPool pool = new ProjectPool();
            Assert.AreNotEqual(pool, null);
        }

        [TestMethod()]
        public void CanCreateProjectTest()
        {
            IProject project = new Project
            {
                Name = "Ceres"
            };
            IProjectPool projectPool = new ProjectPool();
            projectPool.Create(project);

            using(ISession session = sessionFactory.OpenSession())
            {
                IProject readFromDb = session.Get<Project>(project.ID);

                Assert.IsNotNull(readFromDb);
                Assert.AreEqual(readFromDb, project);
            }
        }

        [TestMethod()]
        public void CanDeleteExisting()
        {
            IProject existing = initialProjects[1];
            IProjectPool pool = new ProjectPool();

            pool.Remove(existing);

            IProject readFromDb = pool.GetByID(existing.ID);
            Assert.AreEqual(readFromDb, null);
        }

        [TestMethod()]
        public void CanGetExistingByIDTest()
        {
            IProject existing = initialProjects[0];
            IProjectPool projectPool = new ProjectPool();

            IProject readFromDb = projectPool.GetByID(existing.ID);

            AssertProjectsEqualAndNonNull(readFromDb, existing);

        }
        [TestMethod()]
        public void GettingNonExistingProjectByIdReturnsNull()
        {
            Guid nonExisting = Guid.NewGuid();

            IProjectPool projectPool = new ProjectPool();
            IProject readFromDb = projectPool.GetByID(nonExisting);

            Assert.AreEqual(readFromDb, null);
        }

        [TestMethod()]
        public void GetByNameTest()
        {
            IProject existing = initialProjects[0];
            IProjectPool pool = new ProjectPool();

            IProject readFromDb = pool.GetByName(existing.Name);

            AssertProjectsEqualAndNonNull(readFromDb, existing);
        }
        [TestMethod()]
        public void GettingNonExistingProjectByNameReturnsNull()
        {
            string nonExisting = "aoighodfgareg";

            IProjectPool projectPool = new ProjectPool();
            IProject readFromDb = projectPool.GetByName(nonExisting);

            Assert.AreEqual(readFromDb, null);
        }

        [TestMethod()]
        public void CanUpdateProperties()
        {
            IProject existing = initialProjects[0];
            IProjectPool pool = new ProjectPool();
            existing.Name = "Titan";

            pool.Update(existing);

            IProject readFromDb = pool.GetByID(existing.ID);
            AssertProjectsEqualAndNonNull(readFromDb, existing);
        }
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdatingNullProjectThrowsException()
        {
            IProject nullProject = null;
            IProjectPool pool = new ProjectPool();

            pool.Update(nullProject);
        }

        protected void CreateInitialProjects()
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    foreach(IProject project in initialProjects)
                    {
                        session.Save(project);
                    }
                    transaction.Commit();
                }
            }
                
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="readFromDb">Tested if null</param>
        /// <param name="existing">Assumed to be non-null</param>
        void AssertProjectsEqualAndNonNull(IProject readFromDb, IProject existing)
        {
            Assert.AreNotEqual(readFromDb, null);
            Assert.AreEqual(readFromDb.ID, existing.ID);
            Assert.AreEqual(readFromDb.Name, existing.Name);
        }
    }
}