﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Model;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace ClockTests.System_Configuration
{
    [TestClass()]
    public class NHibernateSetupTests
    {
        [TestMethod()]
        public void CanGenerateSchema()
        {
            var cfg = new Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Project).Assembly);

            new SchemaExport(cfg).Execute(false, true, false);//false, true, false, false);
        }
    }
}
