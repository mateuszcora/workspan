﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clock.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Clock.Configuration.Tests
{
    [TestClass()]
    public class ConfigTests
    {
        static string SettingsFilePath;
        static string RegularName = "settings.cfg";
        static string BackupName = "backup.cfg";

        static string RegularFullName { get { return SettingsFilePath + RegularName; } }
        static string BackupFullName { get { return SettingsFilePath + BackupName; } }

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            string path = Environment.GetEnvironmentVariable("APPDATA");
            path += @"\Workspan\Config\";
            SettingsFilePath = path;
            if (File.Exists(RegularFullName))
            {
                File.Move(RegularFullName, BackupFullName);
            }

             FakeSettings =
                @"<?xml version=""1.0"" encoding=""utf-8""?>" + Environment.NewLine + 
                @"<Config xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + Environment.NewLine + 
                @"<DatabaseLocation>" + FakeDB + @"</DatabaseLocation>" + Environment.NewLine +
                @"<DatabaseUsername>" + FakeUsername + @"</DatabaseUsername>" + Environment.NewLine +
                @"<Password>" + FakePass + @"</Password>" + Environment.NewLine +
                @"<DbName>" + FakeDbName + @"</DbName>" + Environment.NewLine + 
                @"<IntegratedSecurity>" + FakeIntegratedSecurity + @"</IntegratedSecurity>" + Environment.NewLine + 
                @"</Config>";
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            if (File.Exists(BackupFullName))
            {
                if (File.Exists(RegularFullName))
                {
                    File.Delete(RegularFullName);
                }
                File.Move(BackupFullName, RegularFullName);
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if (File.Exists(RegularFullName))
            {
                File.Delete(RegularFullName);
            }
        }
        static string FakeDB = "DummyLocation";
        static string FakeUsername = "FakeLogin";
        static string FakePass = "wronGPasSs123";
        static string FakeDbName = "fakeName";
        static string FakeIntegratedSecurity = "true";
        static string FakeSettings;

        static void GenerateFakeSettingsFile()
        {
            using (Stream stream = new FileStream(RegularFullName, FileMode.OpenOrCreate))
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(FakeSettings);
                }
            }
        }
        
        [TestMethod()]
        public void LoadsSettingsFile()
        {
            GenerateFakeSettingsFile();

            Config config = Config.Load();

            Assert.AreEqual(config.DbPath, FakeDB);
            Assert.AreEqual(config.DbUser, FakeUsername);
            Assert.AreEqual(config.DbPass, FakePass);
            
        }

        [TestMethod()]
        public void CreatesSettingsFileIfNotExists()
        {
            Config config = Config.Load();

            Assert.AreEqual(File.Exists(RegularFullName), true);
            Assert.AreEqual(config.DbPath, null);
            Assert.AreEqual(config.DbPass, null);
            Assert.AreEqual(config.DbUser, null); 
        }

        [TestMethod()]
        public void SavesServerName()
        {
            string NewValue = "agpehrgajkkaetlgkjejthuoWRIHGUAESRNG";
            GenerateFakeSettingsFile();
            Config config = Config.Load();

            config.DbPath = NewValue;
            config.Save();
            config = Config.Load();

            Assert.AreEqual(NewValue, config.DbPath);
        }

        [TestMethod()]
        public void SavesLogin()
        {

            string NewValue = "agpehrgajkkaetlgkjejthuoWRIHGUAESRNG";
            GenerateFakeSettingsFile();
            Config config = Config.Load();

            config.DbUser = NewValue;
            config.Save();
            config = Config.Load();

            Assert.AreEqual(NewValue, config.DbUser);
        }

        [TestMethod()]
        public void SavesPass()
        {

            string NewValue = "agpehrgajkkaetlgkjejthuoWRIHGUAESRNG";
            GenerateFakeSettingsFile();
            Config config = Config.Load();

            config.DbPass = NewValue;
            config.Save();
            config = Config.Load();

            Assert.AreEqual(NewValue, config.DbPass);
        }

    }
}