﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Utils;

namespace Clock.Model
{
    public class Project : GuidedRecord, IProject
    {
        public Project() : base()
        {

        }
        public override string ToString()
        {
            return Name;
        }
        public override int GetHashCode()
        {
            int hash = 5;
            hash = Name.GetHashCode () + hash * 7;
            hash *= ID.GetHashCode() + hash * 7;
            return hash;
        }
        public override bool Equals(object obj)
        {
            Project other = obj as Project;
            if (other == null)
            {
                return false;
            }

            bool equals = true;
            equals &= Name == other.Name;
            equals &= ID == other.ID;

            return equals;
        }
        public virtual string Name { get; set; }
        public virtual HoursMinutes TotalWorkTime
        {
            get
            {
                IWorkspanPool workspanPool = new WorkspanPool();
                IEnumerable < IWorkspan > workspans = workspanPool.GetByProject(this);
                HoursMinutes WorkTime = new HoursMinutes();
                foreach (IWorkspan Workspan in workspans)
                {
                    WorkTime += Workspan.Worktime;
                }
                return WorkTime;
            }
        } 

    }
}
