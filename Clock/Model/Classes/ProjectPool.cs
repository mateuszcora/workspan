﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using Clock.Utils;

namespace Clock.Model
{
    public class ProjectPool : IProjectPool
    {

        public ProjectPool() : base()
        {

        }
        public void Create(IProject project)
        {
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(project);
                    transaction.Commit();
                }
            }

        }

        public void Remove(IProject project)
        {
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Delete(project);
                    transaction.Commit();
                }
            }
        }

        public IProject GetByID(Guid _ID)
        {
            IProject project = null;
            using (ISession session = SessionProvider.OpenSession())
            {
                project = session.Get<Project>(_ID);
            }
            return project;
        }

        public IProject GetByName(string Name)
        {
            IProject project = null;
            using (ISession session = SessionProvider.OpenSession())
            {
                project = session.CreateCriteria(typeof(Project))
                    .Add(Restrictions.Eq("Name", Name))
                    .UniqueResult<Project>();
            }
            return project;
        }

        public void Update(IProject project)
        {
            if (project == null)
                throw new ArgumentNullException("Cannot update null project");
            using(ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Update(project);
                    transaction.Commit();
                }
            }
        }
        /// <summary>
        /// returns number of first results, or all results if number is 0
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public IEnumerable<IProject> GetMany(int number = 0)
        {
            IEnumerable<IProject> projects;
            using (ISession session = SessionProvider.OpenSession())
            {
                using(ITransaction transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<Project>()
                        .SetFirstResult(0);
                    if(number > 0)
                    {
                        criteria.SetMaxResults(number);
                    }
                    projects = criteria.List<IProject>();
                }
            }
            return projects;
        }

        public IEnumerable<IProject> GetAll()
        {
            return GetMany(0);
        }
    }
}
