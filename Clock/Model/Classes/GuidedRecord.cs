﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Model
{
    public class GuidedRecord : IGuidedRecord
    {
        public virtual Guid ID { get; set; }
        public GuidedRecord()
        {
            //guid = Guid.NewGuid();
        }
    }
}
