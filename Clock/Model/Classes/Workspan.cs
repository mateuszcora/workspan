﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Utils;

namespace Clock.Model
{
    public class Workspan : GuidedRecord, IWorkspan
    {
        public Workspan()
        {
            internalStart = DateTime.MinValue;
            internalEnd = DateTime.MinValue;
        }
        public override int GetHashCode()
        {
            int hash = 7;
            hash = StartTime.GetHashCode() + hash * 7;
            hash = EndTime.GetHashCode() + hash * 7;
            hash *= Description == null ? 1 : Description.GetHashCode() + hash * 7;
            //hash *= Project == null ? 1 :  Project.GetHashCode() + hash  * 7;
            hash *= ID == null ? 1 : ID.GetHashCode() + hash * 7;
            return hash;
        }
        public override bool Equals(object obj)
        {
            Workspan other = obj as Workspan;
            if(other == null)
            {
                return false;
            }
            bool equals = true;
            equals &= StartTime.Equals(other.StartTime);
            equals &= EndTime.Equals(other.EndTime);
            equals &= (Description == other.Description);
            equals &= (Project == other.Project);
            equals &= (ID == other.ID);

            return equals;
        }
        public Workspan(DateTime _start, DateTime _end)
        {
            StartTime = _start;
            EndTime = _end;
        }
        public Workspan(DateTime _start, DateTime _end, IProject _project)
        {
            StartTime = _start;
            EndTime = _end;
            Project = _project;
        }
        public virtual IProject Project { get; set; }
        public virtual DateTime StartTime {
            get
            {
                return internalStart;
            }
            set
            {
                
                if (internalEnd != DateTime.MinValue && value >= internalEnd)
                {
                    ThrowWrongTimesException();
                }
                
                internalStart = value;
            }
        }
        public virtual DateTime EndTime {
            get
            {
                return internalEnd;
            }
            set
            {
                
                if (internalStart != DateTime.MinValue && internalStart >= value)
                {
                    ThrowWrongTimesException();
                }
                
                internalEnd = value;
            }
        }
        protected void ThrowWrongTimesException()
        {
            throw new Exception("End time must be later than start time");
        }
        public virtual int Hours
        {
            get
            {
                return (EndTime - StartTime).Hours;
            }
        }
        public virtual int Minutes
        {
            get
            {
                return (EndTime - StartTime).Minutes;
            }
        }
        public virtual HoursMinutes Worktime
        {
            get
            {
                return new HoursMinutes(Hours, Minutes);
            }
        }
        public virtual string Description
        {
            get
            {
                return internalDescription;
            }
            set
            {
                internalDescription = value;
            }
        }
        private string internalDescription;
        private  DateTime internalStart;
        private  DateTime internalEnd;
    }
}
