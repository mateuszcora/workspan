﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;
using NHibernate.Criterion;
using Clock.Utils;

namespace Clock.Model
{
    public class WorkspanPool : IWorkspanPool
    {
        public WorkspanPool() : base()
        {

        }
        public void Create(IWorkspan workspan)
        {
            VerifyAndThrowExceptionIfFails(workspan);
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    VerifyAndThrowExceptionIfFails(workspan);
                    session.Save(workspan);
                    transaction.Commit();
                }
            }
        }
        public void Update(IWorkspan workspan)
        {
            using(ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Update(workspan);
                    transaction.Commit();
                }
            }
        }
        public void Create (IEnumerable<IWorkspan> range)
        {
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    foreach(IWorkspan workspan in range)
                    {
                        VerifyAndThrowExceptionIfFails(workspan);
                        session.Save(workspan);
                    }
                    transaction.Commit();
                }
            }
        }

        public IEnumerable<IWorkspan> GetByProject(IProject project)
        {
           IEnumerable<IWorkspan> workspans = new List<IWorkspan>();
            using(ISession session = SessionProvider.OpenSession())
            {
                using(ITransaction transaction = session.BeginTransaction())
                {
                    workspans = session
                        .CreateCriteria<Workspan>()
                        .CreateCriteria("Project")
                        .Add(Restrictions.Eq("Name", project.Name))
                        //.UniqueResult<IWorkspan>());
                        .List<IWorkspan>();
                }
            }
            return workspans;
        }

        public IWorkspan GetByID(Guid id)
        {
            IWorkspan workspan = null;
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    workspan = session.Get<Workspan>(id);
                }
            }
            return workspan;
        }

        public void Remove(IWorkspan workspan)
        {
            using (ISession session = SessionProvider.OpenSession())
            {
                using(ITransaction transaction = session.BeginTransaction())
                {
                    session.Delete(workspan);
                    transaction.Commit();
                }
            }
        }
        protected void VerifyAndThrowExceptionIfFails(IWorkspan workspan)
        {
            if (workspan.StartTime == null || workspan.EndTime == null || workspan.Project == null)
                throw new Exception("A workspan must have an assigned project and a start time and end time");
        }
        /// <summary>
        /// returns number of first results, or all results if number is 0
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public IEnumerable<IWorkspan> GetMany(int number)
        {
            IEnumerable<IWorkspan> workspans;
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    ICriteria criteria = session.CreateCriteria<Workspan>()
                        .SetFirstResult(0);
                    if (number > 0)
                    {
                        criteria.SetMaxResults(number);
                    }
                    workspans = criteria.List<IWorkspan>();
                }
            }
            return workspans;
        }
    }
}
