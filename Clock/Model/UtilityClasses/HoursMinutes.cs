﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Utils
{
    public class HoursMinutes
    {
        public override int GetHashCode()
        {
            int hash = 47;
            hash = minutes * 13 + hash;
            hash = minutes * 13 + hash;
            return hash;
        }
        public override bool Equals(object obj)
        {
            HoursMinutes other = obj as HoursMinutes;
            if(other == null)
            {
                return false;
            }
            bool equal = true;
            equal &= Hours == other.Hours;
            equal &= Minutes == other.Minutes;
            return equal;
        }
        public int Hours { get; set; }
        public int Minutes
        {
            get
            {
                return minutes;
            }
            set
            {
                if (value >= 60)
                    throw new ArgumentException("The number of minutes is too high");
                minutes = value;
            }
        }
        protected int minutes;
        public HoursMinutes (int hours, int minutes)
        {
            Hours = hours;
            Minutes = minutes;
        }
        public HoursMinutes()
        {
            Hours = 0;
            Minutes = 0;
        }
        public HoursMinutes(TimeSpan TimeSpan)
        {
            Hours = TimeSpan.Hours;
            minutes = TimeSpan.Minutes;
        }
        public static HoursMinutes operator + (HoursMinutes first, HoursMinutes second)
        {
            int TotalHours = first.Hours + second.Hours;
            int TotalMinutes = first.Minutes + second.Minutes;
            while(TotalMinutes >= 60)
            {
                TotalHours++;
                TotalMinutes -= 60;
            }
            return new HoursMinutes(TotalHours, TotalMinutes);
        }
        public override string ToString()
        {
            return Hours.ToString("00") + ":" + Minutes.ToString("00");
        }
    }
}
