﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Utils
{
    public class TimeProvider
    {
        public static DateTime GetCurrentTime()
        {
            return DateTime.Now;
        }
    }
}
