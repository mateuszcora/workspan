﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;
using Clock.Model;
using Clock.Configuration;

namespace Clock.Utils

{
    class SessionProvider
    {
        protected static ISessionFactory _factory;
        private static ISessionFactory SessionFactory
        {
            get
            {
                if(_factory == null)
                {
                    _factory = CreateFactory();
                }
                return _factory;
            }
        }
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
        public static void ResetSession()
        {
            CreateFactory();
        }
        protected static ISessionFactory CreateFactory()
        {
            NHibernate.Cfg.Configuration nHibernateConfiguration = new NHibernate.Cfg.Configuration();
            nHibernateConfiguration.Configure();

            IConfig config = Config.Load();
            nHibernateConfiguration.SetProperty("connection.connection_string", config.ConnectionString);

            nHibernateConfiguration.AddAssembly(typeof(Project).Assembly);

            new SchemaUpdate(nHibernateConfiguration).Execute(true, true);
            //new SchemaExport(configuration).Execute(true, true, false);
            return nHibernateConfiguration.BuildSessionFactory();
        }
    }
}
/*
 * ame="NHibernate.Test">
		<property name="connection.driver_class">NHibernate.Driver.Sql2008ClientDriver</property>
		<property name="connection.connection_string">
      Server=HELL\SQLEXPRESS;initial catalog=nhibernate;Integrated Security=SSPI;Initial Catalog=Production
    </property>
		<property name="dialect">NHibernate.Dialect.MsSql2008Dialect</property>
    <property name="show_sql">true</property>
	</session-factory>
 * 
 * */
