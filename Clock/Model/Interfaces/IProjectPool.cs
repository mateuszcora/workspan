﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Model
{
    public interface IProjectPool
    {
        void Create(IProject project);
        IProject GetByID(Guid _ID);
        IEnumerable<IProject> GetMany(int number);
        IEnumerable<IProject> GetAll();
        IProject GetByName(string Name);
        void Update(IProject project);
        void Remove(IProject project);

    }
}
