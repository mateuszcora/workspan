﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Utils;

namespace Clock.Model
{
    public interface IWorkspan : IGuidedRecord
    {
        DateTime StartTime { get; }
        DateTime EndTime { get; }
        int Hours { get; }
        int Minutes { get; }
        HoursMinutes Worktime { get; }
        string Description { get; set; }
        IProject Project { get; set; }
        string ToString();
    }
}
