﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Model
{
    public interface IWorkspanPool
    {
        IEnumerable<IWorkspan> GetByProject(IProject project);

        IWorkspan GetByID(Guid id);

        IEnumerable<IWorkspan> GetMany(int number);
        void Create(IWorkspan workspan);

        void Create(IEnumerable<IWorkspan> range);

        void Remove(IWorkspan workspan);

        void Update(IWorkspan workspan);


    }
}
