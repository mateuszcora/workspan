﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Utils;

namespace Clock.Model
{
    public interface IProject : IGuidedRecord
    {
        string Name { get; set; }
        string ToString();
        HoursMinutes TotalWorkTime { get; }
    }
}
