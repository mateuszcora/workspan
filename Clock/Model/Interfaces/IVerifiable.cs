﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Model.Interfaces
{
    interface IVerifiable
    {
        bool CanBeSaved();
    }
}
