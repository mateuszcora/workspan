﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Clock.Configuration;


namespace Clock.Utils

{
    public class ErrorLog
    {
        protected static string LogFileName { get => "errors.log"; }
        public static void Log(string error)
        {
            string LogPath = Config.SettingsFolder + @"\" + LogFileName;
            using (FileStream fileStream = new FileStream(LogPath, FileMode.Append))
            {
                using(StreamWriter writer = new StreamWriter(fileStream))
                {
                    writer.WriteLine(DateTime.Now.ToString() + ": " + error);
                }
            }
        }
    }
}
