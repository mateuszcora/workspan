﻿namespace Clock
{
    partial class CreateProjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProjectNameField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ConfirmProjectButton = new System.Windows.Forms.Button();
            this.CancelProjectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ProjectNameField
            // 
            this.ProjectNameField.Location = new System.Drawing.Point(96, 10);
            this.ProjectNameField.Name = "ProjectNameField";
            this.ProjectNameField.Size = new System.Drawing.Size(186, 20);
            this.ProjectNameField.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // ConfirmProjectButton
            // 
            this.ConfirmProjectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConfirmProjectButton.Location = new System.Drawing.Point(12, 43);
            this.ConfirmProjectButton.Name = "ConfirmProjectButton";
            this.ConfirmProjectButton.Size = new System.Drawing.Size(75, 23);
            this.ConfirmProjectButton.TabIndex = 2;
            this.ConfirmProjectButton.Text = "Confirm";
            this.ConfirmProjectButton.UseVisualStyleBackColor = true;
            this.ConfirmProjectButton.Click += new System.EventHandler(this.ConfirmProjectButton_Click);
            // 
            // CancelProjectButton
            // 
            this.CancelProjectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelProjectButton.Location = new System.Drawing.Point(207, 43);
            this.CancelProjectButton.Name = "CancelProjectButton";
            this.CancelProjectButton.Size = new System.Drawing.Size(75, 23);
            this.CancelProjectButton.TabIndex = 3;
            this.CancelProjectButton.Text = "Cancel";
            this.CancelProjectButton.UseVisualStyleBackColor = true;
            this.CancelProjectButton.Click += new System.EventHandler(this.CancelProjectButton_Click);
            // 
            // CreateProjectForm
            // 
            this.AcceptButton = this.ConfirmProjectButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 78);
            this.Controls.Add(this.CancelProjectButton);
            this.Controls.Add(this.ConfirmProjectButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ProjectNameField);
            this.Name = "CreateProjectForm";
            this.Text = "New Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ProjectNameField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ConfirmProjectButton;
        private System.Windows.Forms.Button CancelProjectButton;
    }
}