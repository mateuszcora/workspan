﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clock.Model;

namespace Clock
{
    public partial class SelectProject : Form
    {
        private IProject SelectedProject = null;
        public IProject Selected
        {
            get
            {
                return SelectedProject;
            }
        }
        public SelectProject()
        {
            InitializeComponent();
            SetUpDataGrid();
            ProjectGrid.ClearSelection();
        }

        protected void SetUpDataGrid()
        {
            ProjectGrid.SelectionChanged += SelectionChanged;
            ProjectGrid.CellDoubleClick += GridDoubleClick;

            BindingSource source = new BindingSource();
            IProjectPool pool = new ProjectPool();
            source.DataSource = pool.GetAll();
            ProjectGrid.DataSource = source;
            ProjectGrid.Refresh();
        }

        protected void SelectionChanged(object sender, EventArgs args)
        {
            DataGridView grid = sender as DataGridView;
            if(sender != null)
            {
                DataGridViewSelectedRowCollection selectedRows = grid.SelectedRows;
                if(selectedRows.Count > 1)
                {
                    throw new Exception("Something went wrong, more than one project selected");
                }
                foreach(DataGridViewRow Row in selectedRows)
                {
                    IProject project = Row.DataBoundItem as IProject;
                    if(project != null)
                        SelectedProject = project;
                }
            }
        }
        protected void GridDoubleClick (object sender, EventArgs args)
        {
            AcceptButton.PerformClick();
        }
    }
}
