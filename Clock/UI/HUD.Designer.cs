﻿using Clock.Model;

namespace Clock
{
    partial class HUD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MoreButton = new System.Windows.Forms.Button();
            this.PauseButton = new System.Windows.Forms.Button();
            this.ProjectButton = new System.Windows.Forms.Button();
            this.TaskButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.ProjectsTab = new System.Windows.Forms.TabPage();
            this.RefreshProjects = new System.Windows.Forms.Button();
            this.ProjectListView = new System.Windows.Forms.DataGridView();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iProjectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.WorkspanTab = new System.Windows.Forms.TabPage();
            this.RefreshWorkspans = new System.Windows.Forms.Button();
            this.WorkspanGridView = new System.Windows.Forms.DataGridView();
            this.projectDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.worktimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iWorkspanBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.OptionsTab = new System.Windows.Forms.TabPage();
            this.ExitButton = new System.Windows.Forms.Button();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TimerLabel = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.OptionsConnectionPage = new System.Windows.Forms.TabPage();
            this.ConfigDbPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ConfigDbUser = new System.Windows.Forms.TextBox();
            this.ConfigDbPass = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ConfigDbName = new System.Windows.Forms.TextBox();
            this.ConfigIntegratedSecurity = new System.Windows.Forms.CheckBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.SaveConfigButton = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.ProjectsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iProjectBindingSource)).BeginInit();
            this.WorkspanTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspanGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iWorkspanBindingSource1)).BeginInit();
            this.OptionsTab.SuspendLayout();
            this.OptionsConnectionPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // MoreButton
            // 
            this.MoreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MoreButton.Location = new System.Drawing.Point(540, 15);
            this.MoreButton.Margin = new System.Windows.Forms.Padding(4);
            this.MoreButton.Name = "MoreButton";
            this.MoreButton.Size = new System.Drawing.Size(100, 37);
            this.MoreButton.TabIndex = 0;
            this.MoreButton.Text = "More";
            this.MoreButton.UseVisualStyleBackColor = true;
            this.MoreButton.Click += new System.EventHandler(this.MoreButton_Click);
            // 
            // PauseButton
            // 
            this.PauseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PauseButton.Location = new System.Drawing.Point(432, 15);
            this.PauseButton.Margin = new System.Windows.Forms.Padding(4);
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.Size = new System.Drawing.Size(100, 37);
            this.PauseButton.TabIndex = 1;
            this.PauseButton.Text = "Start";
            this.PauseButton.UseVisualStyleBackColor = true;
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // ProjectButton
            // 
            this.ProjectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ProjectButton.Location = new System.Drawing.Point(16, 15);
            this.ProjectButton.Margin = new System.Windows.Forms.Padding(4);
            this.ProjectButton.Name = "ProjectButton";
            this.ProjectButton.Size = new System.Drawing.Size(200, 37);
            this.ProjectButton.TabIndex = 2;
            this.ProjectButton.Text = "Project";
            this.ProjectButton.UseVisualStyleBackColor = true;
            this.ProjectButton.Click += new System.EventHandler(this.ProjectButton_Click);
            // 
            // TaskButton
            // 
            this.TaskButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TaskButton.Location = new System.Drawing.Point(224, 15);
            this.TaskButton.Margin = new System.Windows.Forms.Padding(4);
            this.TaskButton.Name = "TaskButton";
            this.TaskButton.Size = new System.Drawing.Size(200, 37);
            this.TaskButton.TabIndex = 3;
            this.TaskButton.Text = "Task";
            this.TaskButton.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.ProjectsTab);
            this.tabControl1.Controls.Add(this.WorkspanTab);
            this.tabControl1.Controls.Add(this.OptionsTab);
            this.tabControl1.Location = new System.Drawing.Point(16, 59);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(848, 788);
            this.tabControl1.TabIndex = 0;
            // 
            // ProjectsTab
            // 
            this.ProjectsTab.Controls.Add(this.RefreshProjects);
            this.ProjectsTab.Controls.Add(this.ProjectListView);
            this.ProjectsTab.Controls.Add(this.button1);
            this.ProjectsTab.Location = new System.Drawing.Point(4, 28);
            this.ProjectsTab.Margin = new System.Windows.Forms.Padding(4);
            this.ProjectsTab.Name = "ProjectsTab";
            this.ProjectsTab.Padding = new System.Windows.Forms.Padding(4);
            this.ProjectsTab.Size = new System.Drawing.Size(840, 756);
            this.ProjectsTab.TabIndex = 0;
            this.ProjectsTab.Text = "Projects";
            this.ProjectsTab.UseVisualStyleBackColor = true;
            // 
            // RefreshProjects
            // 
            this.RefreshProjects.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RefreshProjects.Location = new System.Drawing.Point(117, 5);
            this.RefreshProjects.Margin = new System.Windows.Forms.Padding(4);
            this.RefreshProjects.Name = "RefreshProjects";
            this.RefreshProjects.Size = new System.Drawing.Size(100, 37);
            this.RefreshProjects.TabIndex = 3;
            this.RefreshProjects.Text = "Refresh";
            this.RefreshProjects.UseVisualStyleBackColor = true;
            this.RefreshProjects.Click += new System.EventHandler(this.RefreshProjects_Click);
            // 
            // ProjectListView
            // 
            this.ProjectListView.AllowUserToAddRows = false;
            this.ProjectListView.AllowUserToDeleteRows = false;
            this.ProjectListView.AllowUserToResizeColumns = false;
            this.ProjectListView.AllowUserToResizeRows = false;
            this.ProjectListView.AutoGenerateColumns = false;
            this.ProjectListView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProjectListView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.TotalWorkTime});
            this.ProjectListView.DataSource = this.iProjectBindingSource;
            this.ProjectListView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ProjectListView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ProjectListView.Location = new System.Drawing.Point(4, 50);
            this.ProjectListView.Margin = new System.Windows.Forms.Padding(4);
            this.ProjectListView.Name = "ProjectListView";
            this.ProjectListView.ReadOnly = true;
            this.ProjectListView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProjectListView.Size = new System.Drawing.Size(832, 702);
            this.ProjectListView.TabIndex = 2;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // TotalWorkTime
            // 
            this.TotalWorkTime.DataPropertyName = "TotalWorkTime";
            this.TotalWorkTime.HeaderText = "Total worktime";
            this.TotalWorkTime.Name = "TotalWorkTime";
            this.TotalWorkTime.ReadOnly = true;
            this.TotalWorkTime.Width = 150;
            // 
            // iProjectBindingSource
            // 
            this.iProjectBindingSource.DataSource = typeof(Clock.Model.IProject);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(9, 5);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 37);
            this.button1.TabIndex = 1;
            this.button1.Text = "New Project";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.NewProject_Click);
            // 
            // WorkspanTab
            // 
            this.WorkspanTab.Controls.Add(this.RefreshWorkspans);
            this.WorkspanTab.Controls.Add(this.WorkspanGridView);
            this.WorkspanTab.Location = new System.Drawing.Point(4, 28);
            this.WorkspanTab.Margin = new System.Windows.Forms.Padding(4);
            this.WorkspanTab.Name = "WorkspanTab";
            this.WorkspanTab.Size = new System.Drawing.Size(840, 756);
            this.WorkspanTab.TabIndex = 2;
            this.WorkspanTab.Text = "Worktime";
            this.WorkspanTab.UseVisualStyleBackColor = true;
            // 
            // RefreshWorkspans
            // 
            this.RefreshWorkspans.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RefreshWorkspans.Location = new System.Drawing.Point(4, 6);
            this.RefreshWorkspans.Margin = new System.Windows.Forms.Padding(4);
            this.RefreshWorkspans.Name = "RefreshWorkspans";
            this.RefreshWorkspans.Size = new System.Drawing.Size(100, 37);
            this.RefreshWorkspans.TabIndex = 4;
            this.RefreshWorkspans.Text = "Refresh";
            this.RefreshWorkspans.UseVisualStyleBackColor = true;
            this.RefreshWorkspans.Click += new System.EventHandler(this.RefreshWorkspans_Click);
            // 
            // WorkspanGridView
            // 
            this.WorkspanGridView.AutoGenerateColumns = false;
            this.WorkspanGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WorkspanGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.projectDataGridViewTextBoxColumn,
            this.startTimeDataGridViewTextBoxColumn,
            this.endTimeDataGridViewTextBoxColumn,
            this.worktimeDataGridViewTextBoxColumn});
            this.WorkspanGridView.DataSource = this.iWorkspanBindingSource1;
            this.WorkspanGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.WorkspanGridView.Location = new System.Drawing.Point(0, 50);
            this.WorkspanGridView.Margin = new System.Windows.Forms.Padding(4);
            this.WorkspanGridView.Name = "WorkspanGridView";
            this.WorkspanGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WorkspanGridView.Size = new System.Drawing.Size(837, 702);
            this.WorkspanGridView.TabIndex = 1;
            // 
            // projectDataGridViewTextBoxColumn
            // 
            this.projectDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.projectDataGridViewTextBoxColumn.DataPropertyName = "Project";
            this.projectDataGridViewTextBoxColumn.HeaderText = "Project";
            this.projectDataGridViewTextBoxColumn.Name = "projectDataGridViewTextBoxColumn";
            // 
            // startTimeDataGridViewTextBoxColumn
            // 
            this.startTimeDataGridViewTextBoxColumn.DataPropertyName = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.HeaderText = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.Name = "startTimeDataGridViewTextBoxColumn";
            this.startTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // endTimeDataGridViewTextBoxColumn
            // 
            this.endTimeDataGridViewTextBoxColumn.DataPropertyName = "EndTime";
            this.endTimeDataGridViewTextBoxColumn.HeaderText = "EndTime";
            this.endTimeDataGridViewTextBoxColumn.Name = "endTimeDataGridViewTextBoxColumn";
            this.endTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // worktimeDataGridViewTextBoxColumn
            // 
            this.worktimeDataGridViewTextBoxColumn.DataPropertyName = "Worktime";
            this.worktimeDataGridViewTextBoxColumn.HeaderText = "Worktime";
            this.worktimeDataGridViewTextBoxColumn.Name = "worktimeDataGridViewTextBoxColumn";
            this.worktimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iWorkspanBindingSource1
            // 
            this.iWorkspanBindingSource1.DataSource = typeof(Clock.Model.IWorkspan);
            // 
            // OptionsTab
            // 
            this.OptionsTab.Controls.Add(this.tabControl2);
            this.OptionsTab.Location = new System.Drawing.Point(4, 28);
            this.OptionsTab.Margin = new System.Windows.Forms.Padding(4);
            this.OptionsTab.Name = "OptionsTab";
            this.OptionsTab.Padding = new System.Windows.Forms.Padding(4);
            this.OptionsTab.Size = new System.Drawing.Size(840, 756);
            this.OptionsTab.TabIndex = 1;
            this.OptionsTab.Text = "Options";
            this.OptionsTab.UseVisualStyleBackColor = true;
            // 
            // ExitButton
            // 
            this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitButton.Location = new System.Drawing.Point(756, 15);
            this.ExitButton.Margin = new System.Windows.Forms.Padding(4);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(100, 37);
            this.ExitButton.TabIndex = 6;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TotalWorkTime";
            this.dataGridViewTextBoxColumn1.HeaderText = "Total worktime";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Project";
            this.dataGridViewTextBoxColumn2.HeaderText = "Project";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Project";
            this.dataGridViewTextBoxColumn3.HeaderText = "Project";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // TimerLabel
            // 
            this.TimerLabel.Location = new System.Drawing.Point(649, 16);
            this.TimerLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TimerLabel.Name = "TimerLabel";
            this.TimerLabel.Size = new System.Drawing.Size(100, 37);
            this.TimerLabel.TabIndex = 7;
            this.TimerLabel.Text = "00:00";
            this.TimerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(824, 719);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // OptionsConnectionPage
            // 
            this.OptionsConnectionPage.Controls.Add(this.SaveConfigButton);
            this.OptionsConnectionPage.Controls.Add(this.ConfigIntegratedSecurity);
            this.OptionsConnectionPage.Controls.Add(this.ConfigDbName);
            this.OptionsConnectionPage.Controls.Add(this.ConfigDbPass);
            this.OptionsConnectionPage.Controls.Add(this.ConfigDbUser);
            this.OptionsConnectionPage.Controls.Add(this.ConfigDbPath);
            this.OptionsConnectionPage.Controls.Add(this.label5);
            this.OptionsConnectionPage.Controls.Add(this.label4);
            this.OptionsConnectionPage.Controls.Add(this.button3);
            this.OptionsConnectionPage.Controls.Add(this.button2);
            this.OptionsConnectionPage.Controls.Add(this.label3);
            this.OptionsConnectionPage.Controls.Add(this.label2);
            this.OptionsConnectionPage.Controls.Add(this.label1);
            this.OptionsConnectionPage.Location = new System.Drawing.Point(4, 25);
            this.OptionsConnectionPage.Name = "OptionsConnectionPage";
            this.OptionsConnectionPage.Padding = new System.Windows.Forms.Padding(3);
            this.OptionsConnectionPage.Size = new System.Drawing.Size(824, 719);
            this.OptionsConnectionPage.TabIndex = 0;
            this.OptionsConnectionPage.Text = "Connection";
            this.OptionsConnectionPage.UseVisualStyleBackColor = true;
            // 
            // ConfigDbPath
            // 
            this.ConfigDbPath.Location = new System.Drawing.Point(238, 14);
            this.ConfigDbPath.Name = "ConfigDbPath";
            this.ConfigDbPath.Size = new System.Drawing.Size(427, 22);
            this.ConfigDbPath.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "SQL Server:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(114, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Server password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Server login:";
            // 
            // ConfigDbUser
            // 
            this.ConfigDbUser.Location = new System.Drawing.Point(238, 52);
            this.ConfigDbUser.Name = "ConfigDbUser";
            this.ConfigDbUser.Size = new System.Drawing.Size(427, 22);
            this.ConfigDbUser.TabIndex = 4;
            // 
            // ConfigPassword
            // 
            this.ConfigDbPass.Location = new System.Drawing.Point(238, 89);
            this.ConfigDbPass.Name = "ConfigPassword";
            this.ConfigDbPass.Size = new System.Drawing.Size(427, 22);
            this.ConfigDbPass.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(590, 690);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(149, 690);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(114, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Database name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(103, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Integrated security:";
            // 
            // ConfigDbName
            // 
            this.ConfigDbName.Location = new System.Drawing.Point(238, 122);
            this.ConfigDbName.Name = "ConfigDbName";
            this.ConfigDbName.Size = new System.Drawing.Size(427, 22);
            this.ConfigDbName.TabIndex = 10;
            // 
            // ConfigIntegratedSecurity
            // 
            this.ConfigIntegratedSecurity.AutoSize = true;
            this.ConfigIntegratedSecurity.Location = new System.Drawing.Point(238, 151);
            this.ConfigIntegratedSecurity.Name = "ConfigIntegratedSecurity";
            this.ConfigIntegratedSecurity.Size = new System.Drawing.Size(18, 17);
            this.ConfigIntegratedSecurity.TabIndex = 11;
            this.ConfigIntegratedSecurity.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.OptionsConnectionPage);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(4, 4);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(832, 748);
            this.tabControl2.TabIndex = 2;
            this.tabControl2.Selected += new System.Windows.Forms.TabControlEventHandler(this.ConnectionOptionsSelected);
            // 
            // SaveConfigButton
            // 
            this.SaveConfigButton.Location = new System.Drawing.Point(238, 174);
            this.SaveConfigButton.Name = "SaveConfigButton";
            this.SaveConfigButton.Size = new System.Drawing.Size(75, 23);
            this.SaveConfigButton.TabIndex = 12;
            this.SaveConfigButton.Text = "Save";
            this.SaveConfigButton.UseVisualStyleBackColor = true;
            this.SaveConfigButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // HUD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 862);
            this.Controls.Add(this.TimerLabel);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.TaskButton);
            this.Controls.Add(this.ProjectButton);
            this.Controls.Add(this.PauseButton);
            this.Controls.Add(this.MoreButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "HUD";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.ProjectsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProjectListView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iProjectBindingSource)).EndInit();
            this.WorkspanTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WorkspanGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iWorkspanBindingSource1)).EndInit();
            this.OptionsTab.ResumeLayout(false);
            this.OptionsConnectionPage.ResumeLayout(false);
            this.OptionsConnectionPage.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button MoreButton;
        private System.Windows.Forms.Button PauseButton;
        private System.Windows.Forms.Button ProjectButton;
        private System.Windows.Forms.Button TaskButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage ProjectsTab;
        private System.Windows.Forms.TabPage OptionsTab;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView ProjectListView;
        private System.Windows.Forms.BindingSource iProjectBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWorkTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.TabPage WorkspanTab;
        private System.Windows.Forms.DataGridView WorkspanGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button RefreshProjects;
        private System.Windows.Forms.Button RefreshWorkspans;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn projectDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn worktimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource iWorkspanBindingSource1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label TimerLabel;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage OptionsConnectionPage;
        private System.Windows.Forms.Button SaveConfigButton;
        private System.Windows.Forms.CheckBox ConfigIntegratedSecurity;
        private System.Windows.Forms.TextBox ConfigDbName;
        private System.Windows.Forms.TextBox ConfigDbPass;
        private System.Windows.Forms.TextBox ConfigDbUser;
        private System.Windows.Forms.TextBox ConfigDbPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
    }
}

