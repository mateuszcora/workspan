﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clock.Model;
using Clock.Controller;
using NHibernate;
using Clock.Utils;
using Clock.Configuration;

namespace Clock
{
    public partial class HUD : Form
    {

        private bool isHud = false;
        protected bool isCountingTime = false;
        private int HeightHud = 90;//56;
        private int HeightFull = 700;
        private Timer Timer;

        public ITimeTracker Tracker { get; set; }

        public HUD()
        {
            InitializeComponent();
        }

        public void Initialize()
        {
            IProjectPool pool = new ProjectPool();
            ProjectListView.DataSource = pool.GetMany(0);
            WorkspanGridView.CellFormatting += ForceWorkspanGridToShowProjectName;

            Timer = new Timer();
            Timer.Interval = 500;
            Timer.Tick += TickPassed;
            Timer.Start();

            // TODO 
            Config config = Config.Load();
            DisplayConfigOnUI(config);
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Tracker.Finish();
            Close();
        }

        private void MoreButton_Click(object sender, EventArgs e)
        {
            if (isHud)
            {
                Height = HeightFull;
                isHud = false;
                MoreButton.Text = "Less";
            }
            else
            {
                Height = HeightHud;
                isHud = true;
                MoreButton.Text = "More";
            }
        }

        private void NewProject_Click(object sender, EventArgs e)
        {
            IProject NewProject = new Project();
            CreateProjectForm projectForm = new CreateProjectForm();
            DialogResult dialogResult = projectForm.ShowDialog(this);
            if(dialogResult == DialogResult.Cancel)
            {
                return;
            }
            else
            {
                IProject project = new Project();
                project.Name = projectForm.ProjectName;
                ProjectPool pool = new ProjectPool();
                pool.Create(project);
            }
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button pauseButton = sender as Button;
                if (!Tracker.IsPaused)
                {
                    pauseButton.Text = "Start";
                    WorkspanDescription descriptionWindow = new WorkspanDescription();
                    DialogResult result = descriptionWindow.ShowDialog();
                    if(result == DialogResult.OK)
                    {
                        Tracker.Description += descriptionWindow.Description;
                    }
                    Tracker.Pause();
                }
                else
                {
                    Tracker.Start();
                    pauseButton.Text = "Pause";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ProjectButton_Click(object sender, EventArgs e)
        {
            SelectProject SelectProjectForm = new SelectProject();
            DialogResult result = SelectProjectForm.ShowDialog();

            if (SelectProjectForm.Selected != null && result == DialogResult.OK)
            {
                Tracker.Project = SelectProjectForm.Selected;
                Button button = sender as Button;
                button.Text = Tracker.Project.Name;
            }
        }

        private void RefreshWorkspans_Click(object sender, EventArgs e)
        {
            using (ISession session = SessionProvider.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    BindingSource source = new BindingSource();
                    IWorkspanPool pool = new WorkspanPool();
                    source.DataSource = pool.GetMany(0);
                    WorkspanGridView.DataSource = source;
                    WorkspanGridView.Refresh();
                    
                }
            }
        }

        private void RefreshProjects_Click(object sender, EventArgs e)
        {
            BindingSource source = new BindingSource();
            IProjectPool pool = new ProjectPool();
            source.DataSource = pool.GetMany(0);
            ProjectListView.DataSource = source;
            ProjectListView.Refresh();
        }
        private void ForceWorkspanGridToShowProjectName(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if(e.Value != null )
            {
                e.Value = e.Value.ToString();
            }
        }
        protected void TickPassed(object sender, EventArgs args)
        {
            HoursMinutes current = Tracker.CurrentWorkingTime;
            TimerLabel.Text = current.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Config config = LoadConfigFromUI();
            config.Save();
        }

        private Config LoadConfigFromUI()
        {
            Config config = new Config();
            config.DbPath = ConfigDbPath.Text;
            config.DbName = ConfigDbName.Text;
            config.DbPass = ConfigDbPass.Text;
            config.DbUser = ConfigDbUser.Text;
            config.IsIntegratedSecurity = ConfigIntegratedSecurity.Checked;

            return config;
        }

        private void DisplayConfigOnUI(Config config)
        {
            if (config == null)
                throw new ArgumentNullException("Cannot display null config");
            ConfigDbName.Text = config.DbName;
            ConfigDbPath.Text = config.DbPath;
            ConfigDbPass.Text = config.DbPass;
            ConfigDbUser.Text = config.DbUser;
            ConfigIntegratedSecurity.Checked = config.IsIntegratedSecurity;
        }
        private void ConnectionOptionsSelected(object sender, TabControlEventArgs e)
        {
            Config config = Config.Load();
            DisplayConfigOnUI(config);
        }
    }
}
