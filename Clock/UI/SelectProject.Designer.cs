﻿using Clock.Model;

namespace Clock
{
    partial class SelectProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProjectGrid = new System.Windows.Forms.DataGridView();
            this.SelectProjectButton = new System.Windows.Forms.Button();
            this.CancelSelection = new System.Windows.Forms.Button();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalWorkTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iProjectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ProjectGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iProjectBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ProjectGrid
            // 
            this.ProjectGrid.AllowUserToAddRows = false;
            this.ProjectGrid.AllowUserToDeleteRows = false;
            this.ProjectGrid.AllowUserToResizeRows = false;
            this.ProjectGrid.AutoGenerateColumns = false;
            this.ProjectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProjectGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.totalWorkTimeDataGridViewTextBoxColumn});
            this.ProjectGrid.DataSource = this.iProjectBindingSource;
            this.ProjectGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.ProjectGrid.Location = new System.Drawing.Point(13, 13);
            this.ProjectGrid.MultiSelect = false;
            this.ProjectGrid.Name = "ProjectGrid";
            this.ProjectGrid.ReadOnly = true;
            this.ProjectGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ProjectGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ProjectGrid.Size = new System.Drawing.Size(500, 235);
            this.ProjectGrid.TabIndex = 0;
            // 
            // SelectProjectButton
            // 
            this.SelectProjectButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SelectProjectButton.Location = new System.Drawing.Point(13, 255);
            this.SelectProjectButton.Name = "SelectProjectButton";
            this.SelectProjectButton.Size = new System.Drawing.Size(75, 23);
            this.SelectProjectButton.TabIndex = 1;
            this.SelectProjectButton.Text = "Select";
            this.SelectProjectButton.UseVisualStyleBackColor = true;
            // 
            // CancelSelection
            // 
            this.CancelSelection.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelSelection.Location = new System.Drawing.Point(438, 255);
            this.CancelSelection.Name = "CancelSelection";
            this.CancelSelection.Size = new System.Drawing.Size(75, 23);
            this.CancelSelection.TabIndex = 2;
            this.CancelSelection.Text = "Cancel";
            this.CancelSelection.UseVisualStyleBackColor = true;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Project";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // totalWorkTimeDataGridViewTextBoxColumn
            // 
            this.totalWorkTimeDataGridViewTextBoxColumn.DataPropertyName = "TotalWorkTime";
            this.totalWorkTimeDataGridViewTextBoxColumn.HeaderText = "Work time";
            this.totalWorkTimeDataGridViewTextBoxColumn.Name = "totalWorkTimeDataGridViewTextBoxColumn";
            this.totalWorkTimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iProjectBindingSource
            // 
            this.iProjectBindingSource.DataSource = typeof(Clock.Model.IProject);
            // 
            // SelectProject
            // 
            this.AcceptButton = this.SelectProjectButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 285);
            this.Controls.Add(this.CancelSelection);
            this.Controls.Add(this.SelectProjectButton);
            this.Controls.Add(this.ProjectGrid);
            this.Name = "SelectProject";
            this.Text = "SelectProject";
            ((System.ComponentModel.ISupportInitialize)(this.ProjectGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iProjectBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ProjectGrid;
        private System.Windows.Forms.BindingSource iProjectBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalWorkTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button SelectProjectButton;
        private System.Windows.Forms.Button CancelSelection;



        
    }
}