﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clock
{
    public partial class WorkspanDescription : Form
    {
        public string Description { get; set; }
        public WorkspanDescription()
        {
            InitializeComponent();
        }
        public void SetDescription(string text)
        {
            DescripitonTextBox.Text = text;
        }

        private void AcceptButton_Click(object sender, EventArgs e)
        {
            Description = DescripitonTextBox.Text;
        }
    }
}
