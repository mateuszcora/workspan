﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clock.Model;
using Clock.Controller;
using System.Diagnostics;
using Clock.Utils;

namespace Clock
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            HUD hud = new HUD();
            hud.Initialize();

            ITimeTracker tracker;
            tracker = new TimeTracker(TimeTracker.CreateDefaultStopwatch, TimeProvider.GetCurrentTime);
            hud.Tracker = tracker;

            Application.Run(hud);
        }
    }
}
