﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clock.Configuration
{
    interface IConfig
    {
        string DbPath { get; set; }
        string DbName { get; set; }
        string DbUser { get; set; }
        string DbPass { get; set; }
        string ConnectionString { get; }
        bool IsIntegratedSecurity { get; set; }
        
        void Save();



    }
}
