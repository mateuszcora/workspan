﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Clock.Utils;
using System.IO;

namespace Clock.Configuration
{
    [XmlRoot("Config")]
    public class Config : IConfig
    {
        public static string EnvVariablePath { get => "APPDATA"; }
        public static string SettingsSubfolder { get => @"\Workspan\Config"; }
        public static string FileName { get => "settings.cfg"; }
        public static string SettingsFolder { get => Environment.GetEnvironmentVariable(EnvVariablePath) + SettingsSubfolder; }


        [XmlElement("DatabaseLocation")]
        public string DbPath { get; set; }
        [XmlElement("DbName")]
        public string DbName { get; set; }
        [XmlElement("DatabaseUsername")]
        public string DbUser { get; set; }
        [XmlElement("Password")]
        public string DbPass { get; set; }
        [XmlElement("IntegratedSecurity")]
        public bool IsIntegratedSecurity { get; set; }
        public string ConnectionString
        {
            get
            {
                string connection = "";
                connection += "Server=" + DbPath + ";";
                connection += "Initial Catalog=" + DbName + ";";
                if (IsIntegratedSecurity)
                {
                    connection += "Integrated Security=SSPI;";
                }
                else
                {
                    connection += "User ID=" + DbUser + ";";
                    connection += "Password=" + DbPass + ";";
                }
                return connection;
            }
        }
        public static Config Load()
        {
            Config config = null;
            try
            {
                config = LoadFromFile();
            }
            catch(Exception ex)
            {
                ErrorLog.Log("Problem while reading configuration, generating default one... " + Environment.NewLine + ex.Message);
                config = GenerateEmpty();
            }
            return config;
        }

        public void Save()
        {
            using (TextWriter writer = new StreamWriter(GetSettingsPathCreateIfNotExists()))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));
                serializer.Serialize(writer, this);
            }
        }

        protected static Config LoadFromFile()
        {
            Config config = null;
            using (TextReader reader = GetReader())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Config));
                config = serializer.Deserialize(reader) as Config;
            }
            return config;
        }
        protected static Config GenerateEmpty()
        {
            Config config = new Config();
            config.Save();   
            return config;
        }

        protected static TextReader GetReader()
        {
            string path = GetSettingsPathCreateIfNotExists();
            TextReader reader = new StreamReader(path);
            return reader;
        }

        protected static string GetSettingsPathCreateIfNotExists()
        {
            string path = SettingsFolder;
            if (!Directory.Exists(path))
            {

                Directory.CreateDirectory(path);
            }
            path += @"\" + FileName;
            return path;
        }

    }
}
