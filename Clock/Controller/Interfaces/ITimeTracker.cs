﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Model;
using Clock.Utils;

namespace Clock.Controller
{
    public interface ITimeTracker
    {
        IProject Project { get; set; }

        bool IsPaused { get;}
        HoursMinutes CurrentWorkingTime { get; }
        HoursMinutes CurrentPauseTime { get; }
        void Pause();
        void Start(IProject project);
        void Start();
        void Finish();
        string Description { get; set; }
    }
}
