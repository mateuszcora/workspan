﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Model;
using System.Windows.Forms;

namespace Clock.Controller
{
    public interface IUIControler
    {

        
        DataGridView ProjectView { get; set; }
        DataGridView WorkspanView { get; set; }
        /* HUD */
        event EventHandler Pause;
        event EventHandler ChooseProject;
        event EventHandler ChooseTask;

        /* Projects */
        Button NewProjectButton { get; set; }
        Button EditProjectButton { get; set; }


        event EventHandler UpdateWorkspanGrid;
        event EventHandler WorkspanChanged;
        event EventHandler ProjectChanged;
        event EventHandler Track;

    }
}
