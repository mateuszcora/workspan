﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clock.Model;
using Clock.Utils;
using System.Diagnostics;
using System.Windows.Forms;

namespace Clock.Controller
{
    public class TimeTracker : ITimeTracker
    {
        public static IStopwatch CreateDefaultStopwatch()
        {
            IStopwatch watch = new Clock();
            return watch;
        }


        protected IProject current { get; set; }
        protected bool isPaused;
        protected IStopwatch WorkTimer { get; set; }
        protected IStopwatch PauseTimer { get; set; }

        protected StopwatchProvider CreateWatch ;
        protected TimeProvider CurrentTime;

        protected DateTime Started { get; set; }
        protected DateTime Ended { get; set; }
        protected HoursMinutes PauseDuration { get; set; }
        protected bool IsRunning { get => WorkTimer.IsRunning; }
        public bool IsPaused { get => !WorkTimer.IsRunning; }

        public TimeTracker(StopwatchProvider stopwatchCreator, TimeProvider provider)
        {
            CreateWatch = stopwatchCreator;
            PauseTimer = CreateWatch();
            WorkTimer = CreateWatch();
            
            CurrentTime = provider;
            Description = "";
            Started = DateTime.MinValue;
            Ended = DateTime.MaxValue;
        }
        
        public static DateTime DefaultTimeProvider()
        {
            return DateTime.Now;
        }
        public delegate IStopwatch StopwatchProvider();
        public delegate DateTime TimeProvider();
        public IProject Project
        {
            get
            {
                return current;
            }
            set
            {
                if(IsRunning)
                    Finish();
                current = value;
            }

        }
        public string Description { get; set; }
       
        public HoursMinutes CurrentWorkingTime
        {
            get
            {
                return new HoursMinutes(WorkTimer.Elapsed);
            }
        }
        public HoursMinutes CurrentPauseTime
        {
            get
            {
                return new HoursMinutes(PauseTimer.Elapsed);
            }
        }
        public void Finish()
        {
            if (IsRunning)
            {
                Ended = CurrentTime();
                SaveCurrentWorkspan();

                WorkTimer = CreateWatch();
                PauseTimer = CreateWatch();
            }
        }

        public void Pause()
        {
            if (WorkTimer.IsRunning)
            {

                Ended = CurrentTime();
                WorkTimer.Stop();
                SaveCurrentWorkspan();
            }
            else
            {
                Started = CurrentTime();
                WorkTimer.Start();
            }

        }

        public void Start(IProject project)
        {
            if (IsRunning)
                Finish();
            current = project;
            Start();
        }
        public void Start()
        {
            if (current == null)
            {
                throw new Exception("Cannot start tracking time without a selected project");
            }
            if (WorkTimer.IsRunning)
            {
                return;
            }
            WorkTimer.Start();
            Started = CurrentTime();
            Ended = DateTime.MaxValue;
        }
        protected void SaveCurrentWorkspan()
        {
            IWorkspanPool workspanPool = new WorkspanPool();
            IWorkspan currentWorkspan = CreateWorkspan();
            workspanPool.Create(currentWorkspan);
            Started = DateTime.MaxValue; 
        }
        protected IWorkspan CreateWorkspan()
        {
            return new Workspan
            {
                StartTime = Started,
                EndTime = Ended,
                Project = Project,
                Description = Description
            };
        }
    }
}
